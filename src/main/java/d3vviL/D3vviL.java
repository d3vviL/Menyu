// WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW// WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW// WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW// WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW// WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW// WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW// WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW// WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW// WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW// WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW// WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW// WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW// WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW// WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW// WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW// WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW// WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW// WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW
package d3vviL;


public final class D3vviL {
	private final ErrorMessage E = new ErrorMessage();
	private final MainService gg = MainService.instance;
	
	public static Context ctx;
	
	/*
	 * this array holds the name of the games this mod menu is
	 * targeting. it is assigned to setItems in init().
	*/
	public String[] games = new String[4];
	
	/*
	 * this array holds special pointers called instances which
	 * hold the data for the games.
	*/
	public GameData[] gameData = new GameData[4];
	
	/*
	 * simple class to house all the error messages, that way i don't
	 * have to keep on re-writing them over and over
	*/
	public class ErrorMessage {
		
	}
	
	// class housing all of the game data
	public static final class Games {
		
		/*
		 * every class that follows must extend this class or
		 * in other words, "inherit" this class. by extending it
		 * they each have access to their own collection of data
		 * declared by this class.
		*/
		public class GameData {
			public String version;
			public Plugin plugin;
			public String pkg;
			public String name;
			public boolean is64Bit;
		}
		
		public static final class Sgdz extends GameData {
			public Sgdz() {
				this.version = "2.9.0";
				this.plugin  = new ShadowgunDeadzone(D3vviL.ctx);
				this.pkg     = "com.madfingergames.deadzone";
				this.name    = "Shadowgun: Deadzone";
				this.is64Bit = false;
			}
		}
		
		public static final class Cc extends GameData {
			public Cc() {
				this.version = "1.4.42";
				this.plugin  = new CastleClash(D3vviL.ctx);
				this.pkg     = "com.igg.castleclash";
				this.name    = "Castle Clash";
				this.is64Bit = false;
			}
		}
		
		public static final class Sk extends GameData {
			public Sk() {
				this.version = "1.7.10";
				this.plugin  = new SoulKnight(D3vviL.ctx);
				this.pkg     = "com.ChillyRoom.DungeonShooter";
				this.name    = "Soul Knight";
				this.is64Bit = false;
			}
		}
		
		public static final class Neb extends GameData {
			public Neb() {
				this.version = "2.1.5";
				this.plugin  = new Nebulous(D3vviL.ctx);
				this.pkg     = "software.simplicial.nebulous";
				this.name    = "Nebulous";
				this.is64Bit = false;
			}
		}
	}
	
	public D3vviL() {
		this.ctx = Tools.getContext();
		
		this.gameData.add(new Games.Sgdz());
		this.gameData.add(new Games.Cc());
		this.gameData.add(new Games.Sk());
		this.gameData.add(new Games.Neb());
	}
	
	/*
	 * sorts the array "games" based on the currently running app.
	 * the app that's currently running is pushed to the top of the
	 * list. then, the old value that was at the top takes the place
	 * at which the new top value was indexed at.
	*/
	private String[] sortGames() {
		ArrayList<GameData> gd = new ArrayList<GameData>();
		
		gd.add(new Games.Sgdz());
		gd.add(new Games.Cc());
		gd.add(new Games.Sk());
		gd.add(new Games.Neb());
		
		ProcessInfo proc = this.gg.processInfo;
		String pkg;
		
		if(!proc) {
			Tools.showToast(E.SELECT_APP_FIRST);
			return null;
		} else { pkg = proc.packageName; }
		
		int gLen = this.games.length;
		boolean skip = false;
		
		for(byte i = 0; i <= gLen; i++) {
			// if i == the length of the games array+1 then that means the game is not supported
			// as it is not in the list of supported games.
			if(i == gLen) {
				Tools.showToast(E.GAME_NOT_SUPPORTED);
				return null;
			}
			
			GameData game = (GameData) gd.get(i);
			
			if(!skip) {
				if(pkg.equals(game.pkg)) {
					GameData old = (GameData) gd.get(0);
					
					gd.add(0, game);
					gd.add(i, old);
					
					skip = true; // skip this block once we got the current game
				}
			} else { this.games[i] = game.name; }
		}
		
		return this.games;
	}
	
	// initialization is done here
	public static void init() {
		final D3vviL thiz = new D3vviL();
		String[] games = thiz.sortGames();
		
		// hide gg
		thiz.gg.dismissDialog();
		thiz.gg.showApp.hideUi();
		
		if(!games) {
			return;
		}
		
		// build the main menu
		Builder mainMenu = Alert.create()
				.setCustomTitle("Main Menu - Select the game")
				.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int btnId) {
						Tools.dismiss(dialog);
					}
				})
				.setItems(thiz.sortGames(), new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int itemId) {
						final GameData game = (GameData) thiz.gameData[itemId];
						Tools.dismiss(dialog);
						
						switch(itemId) {
							case 0:
								// if they click 0, the item at the top of the list,
								// then they're choosing the current game because of the
								// way the games array is sorted in sortGames().
								game.plugin.start();
								break;
							
							// the following two are NOT blank. they just all chain to be resolved
							// at case 3, which is the last link in this "chain". 
							case 1:
							case 2:
							case 3:
								if(Tools.isPackageInstalled(game.pkg)) {
									Builder startApp = Alert.create()
										.setCustomTitle(String.format("Start %s", game.name))
										.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
											@Override
											public void onClick(DialogInterface dialog, int btnId) {
												// if the user clicks "Yes" then menyu will close the current
												// app and start the selected one. Normally, this would end
												// menyu as well as GG would end the thread in which the script,
												// or in this case menyu, is running inside of to prevent the
												// script from trying to access non-existing memory. So i had to
												// modify GG(again) to remove that and to prevent GG from showing
												// the "Game is dead" dialog. this makes switching apps in GG seem
												// seamless, with no user interaction at all.
												
												Tools.showToast("Switching apps...");
												
												// kill the current app GG is attached to
												thiz.gg.processKill();
												
												// start the selected app
												try {
													thiz.ctx.startActivity(
														thiz.ctx.getPackageManager()
														.getLaunchIntentForPackage(game.pkg)
													);
												} catch(NameNotFoundException ex) {
													Tools.showToast(E.COULD_NOT_START_ACTIVITY); // wtf?
													return;
												}
												
												// attempt to get the process/memory info for the app
												ActivityManager manager = (ActivityManager) thiz.getSystemService("activity");
												List<ActivityManager.RunningAppProcessInfo> procs = manager.getRunningAppProcesses();
												int targetPid = 0;
												int targetUid = 0;
												
												// there's no need to create a new int variable
												for(;targetPid < procs.size(); targetPid++) {
													ActivityManager.RunningAppProcessInfo process = (ActivityManager.RunningAppProcessInfo) procs.get(targetPid);
													
													// if we're just gonna break out of the loop once we got the
													// pid and the uid we wanted and assign it accordingly
													if(process.processName.equalsIgnoreCase(game.pkg)) {
														targetPid = process.pid;
														targetUid = process.uid;
														
														break;
													}
												}
												
												// now that we got the PID we must query it's memory/process information
												// this is needed so GG can figure out which process to attach to
												
												// set the app info
												AppInfo appInfo = new AppInfo(targetUid, game.pkg);
												
												// cmdline is always the package name of the target app unless you're
												// inspecting an actual command line application that was started with
												// some arguments. those arguments would go in the file /proc/[pid]/cmdline.
												// for the sake of "optimization" i won't be creating this string as
												// it will be just a waste since i'll just be recreating the game.pkg
												// variable.
												
												// order doesn't matter, this would be the order it's presented in
												// in the app list whenever you start GG. just to be safe set it to
												// 0.
												int order = 0;
												
												// the next parameter in the list would have to be to determine if the
												// app supports or runs in 64-bit mode. this would be a huge waste of
												// time and a resource consuming operation since to do that we'd need to
												// look inside the target apk, look inside it and have a look at the lib/
												// directory to see if it does support running in 64 bit mode. Most of the
												// time the target app won't, especially if it's using il2cpp. so this
												// parameter is predefined in the game's data class represented by the
												// variable 'is64Bit'.
												
												// now we need to get the Residential Set Size(rss) of the target process.
												// the rss somewhat represents the total amount of RAM this process is
												// using. in gg, this would appear as the green number next to the app name,
												// next to the target app icon.
												
												// first we must query the memory info from /proc[pid]/statm
												MemStatReader memStats = new MemStatReader(targetPid);
												long rss = memStats.getStat(Stat.RESIDENT/*SET SIZE(RSS)*/);
												
												// NOW we build the process info
												ProcessInfo proc = new ProcessInfo(
																				appInfo,
																				targetPid,
																				targetUid,
																				game.pkg,
																				order,
																				game.is64Bit,
																				rss
												);
												
												// now we update the process info for gg
												thiz.gg.setProcessInfo(proc);
												
												// start the plugin and we're done here.
												game.plugin.start();
											}
										})
										.setMessage(String.format("Are you sure you want to start %s? This will close the current app, and your unsaved progress may be lost!", game.name)
										.setNegativeButton("No", new DialogInterface.OnClickListener() {
											@Override
											public void onClick(DialogInterface dialog, int btnId) {
												// if the user clicked "No" then close the dialog and start the
												// plugin for the current game.
												dialog.dismiss();
												game.plugin.start();
											}
										})
										.setCancelable(false);
									
									AlertDialog startApp = startApp.create();
									
									Alert.show(startApp);
								}
								
								break;
						}
					}
				})
				.setCancelable(false);
			
			AlertDialog mainMenu = mainMenu.create();
			
			Alert.show(mainMenu);
		} // this closes wrong, but i'm too lazy to figure it out since this method is massive.
	} // will fix later
}