package d3vviL.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

// simple class to parse the file /proc[pid]/statm for
// obtaining memory information about a specific PID.
public final class MemStatReader {
	
	// this holds all the memory data from the statm file.
	private final List<String> memInfo = new ArrayList<String>();
	
	// simple data class(enum) that represents each piece
	// of data in the statm file.
	public enum Stat {
		// from http://man7.org/linux/man-pages/man5/proc.5.html
		SIZE(1),
		RESIDENT(2), // basically the amount of RAM this PID is using.
		SHARED(3),
		TEXT(4),
		LIB(5), // always 0
		DATA(6),
		DT(7); // always 0
		
		private int index;
		
		private Stat(int idx) {
			this.index = idx;
		}
		
		public int getIndex() {
			return this.index;
		}
	}
	
	public MemStatReader(int pid) {
		File statFile = new File(String.format("/proc/%d/statm", pid));
		FileInputStream fis = new FileInputStream(statFile));
		
		try {
			int currentByte = 0;
			StringBuilder sb = new StringBuilder();
			
			// reads a whole "word" into the String builder 'sb'.
			// after a space has been read by the parser, 'sb' will
			// be built into an actual string and then added to the
			// memInfo list to be stored.
			while((currentByte = fis.read()) != -1) {
				if(currentByte == 20) { // 20 is the ascii rep. of a space.
					// in the case that more than one space is parsed, sb could be null
					if(sb != null) {
						
						// build the string and add it to memInfo. if sb was
						// null then this would cause a NullPointerException
						// some where down the line and it will insert a 
						// blank into memInfo, throwing off it's accuracy(
						// this means that getStat() will return unreliable
						// data).
						this.memInfo.add(sb.toString());
						
						// now we basically wipe or clear the string builder so
						// it can be used to build another string without creating
						// a new instance of it, which would be poor programming.
						sb.setLength(0);
					}
					
					continue; // reset the loop, skipping over the current space
				}
				
				// add the current byte to the string builder as a char
				sb.append((char)currentByte);
			}
			
			fis.close();
		} catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	public String getStat(Stat type) {
		return (String) this.memInfo.get(type.getIndex());
	}
}